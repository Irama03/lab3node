const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Truck = new Schema({
  created_by: {type: Schema.Types.ObjectId, required: true},
  assigned_to: {type: String, required: true, default: ' '},
  type: {type: String, required: true},
  status: {type: String, required: true, default: 'IS'},
  created_date: {type: Date, required: true, default: Date.now()},
});
module.exports = mongoose.model('Truck', Truck);
