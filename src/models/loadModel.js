const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Dimension = new Schema({
  width: {type: Number, required: true},
  length: {type: Number, required: true},
  height: {type: Number, required: true},
}, {_id: false});

const Log = new Schema({
  message: {type: String, required: true},
  time: {type: Date, required: true, default: Date.now()},
}, {_id: false});

const defaultLog = {message: 'Load created'};

const Load = new Schema({
  created_by: {type: Schema.Types.ObjectId, required: true},
  assigned_to: {type: String, required: true, default: ' '},
  status: {type: String, required: true, default: 'NEW'},
  state: {type: String, required: true, default: ' '},
  name: {type: String, required: true},
  payload: {type: Number, required: true},
  pickup_address: {type: String, required: true},
  delivery_address: {type: String, required: true},
  dimensions: {type: Dimension, required: true},
  logs: {type: [Log], required: true, default: [defaultLog]},
  created_date: {type: Date, required: true, default: Date.now()},
});
module.exports = mongoose.model('Load', Load);
