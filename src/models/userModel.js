const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const User = new Schema({
  role: {type: String, required: true},
  email: {type: String, required: true, unique: true},
  created_date: {type: Date, required: true, default: Date.now()},
  password: {type: String, required: true},
});
module.exports = mongoose.model('User', User);
