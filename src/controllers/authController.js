const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

// eslint-disable-next-line max-len
const {createCredentials, loginWithCredentials} = require('../services/authService');
const {sendPassword} = require('../services/authService');

router.post('/register', async (req, res) => {
  try {
    const message = await createCredentials(req.body);
    if (message === 200) {
      res.status(200).json({message: 'Profile created successfully'});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.post('/login', async (req, res) => {
  try {
    const message = await loginWithCredentials(req.body);
    if (Array.isArray(message)) {
      res.status(200).json({jwt_token: message[0]});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.post('/forgot_password', async (req, res) => {
  try {
    const message = await sendPassword(req.body);
    if (message === 200) {
      res.status(200).json({message: 'New password sent to your ' +
            'email address'});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

module.exports = router;
