const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const {getUserById, deleteUser, updateUserPassword} =
    require('../services/userService');

router.get('/me', async (req, res) => {
  try {
    const userId = req.userId;
    const user = await getUserById(userId);
    if (!user) {
      res.status(400).json({message: 'There is no info about this user'});
      return;
    }
    res.json({user});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.delete('/me', async (req, res) => {
  try {
    const userId = req.userId;
    const message = await deleteUser(userId, req.userRole === 'DRIVER');
    if (message === 200) {
      res.status(200).json({message: 'Profile deleted successfully'});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.patch('/me/password', async (req, res) => {
  try {
    const userId = req.userId;
    const message = await updateUserPassword(userId, req.body,
        req.userRole === 'DRIVER');
    if (message === 200) {
      res.status(200).json({message: 'Password changed successfully'});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

module.exports = router;
