const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const {getLoadsDriver, getLoadsShipper, createLoad, getActiveLoad,
  setActiveLoadNextState, getLoadById, updateLoad, deleteLoad, postLoad,
  getLoadShippingInfo} = require('../services/loadService');

router.get('/', async (req, res) => {
  try {
    const userId = req.userId;
    let status = 'ALL';
    let limit = 10;
    let offset = 0;

    if (req.query.status) {
      status = req.query.status;
      if (status !== 'NEW' && status !== 'POSTED' && status !== 'ASSIGNED' &&
        status !== 'SHIPPED') {
        res.status(400).json({
          message: 'Status should have one of the values: ' +
              'NEW, POSTED, ASSIGNED, SHIPPED',
        });
        return;
      }
    }
    if (req.query.offset) {
      offset = parseInt(req.query.offset, 10);
      if (Number.isNaN(offset) || offset < 0) {
        res.status(400).json({message: 'Offset should be a number'});
        return;
      }
    }
    if (req.query.limit) {
      limit = parseInt(req.query.limit, 10);
      if (Number.isNaN(limit) || limit < 0 || limit > 50) {
        res.status(400).json({message: 'Limit should be a number <= 50'});
        return;
      }
    }
    if (req.userRole === 'DRIVER') {
      if (status !== 'ALL' && status !== 'ASSIGNED' && status !== 'SHIPPED') {
        res.status(400).json({message: 'Status for driver should have ' +
                'one of the values: ASSIGNED, SHIPPED'});
        return;
      }
      const loads = await getLoadsDriver(userId, status, offset, limit);
      res.status(200).json({loads});
    } else {
      const loads = await getLoadsShipper(userId, status, offset, limit);
      res.status(200).json({loads});
    }
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.post('/', async (req, res) => {
  try {
    const userId = req.userId;
    if (req.userRole !== 'SHIPPER') {
      res.status(400).json({message: 'Access forbidden'});
      return;
    }
    const message = await createLoad(userId, req.body);
    if (message === 200) {
      res.status(200).json({message: 'Load created successfully'});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.get('/active', async (req, res) => {
  try {
    const userId = req.userId;
    if (req.userRole !== 'DRIVER') {
      res.status(400).json({message: 'Access forbidden'});
      return;
    }
    const load = await getActiveLoad(userId);
    if (!load) {
      res.status(400)
          .json({message: 'No active load found'});
      return;
    }
    res.status(200).json({load});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.patch('/active/state', async (req, res) => {
  try {
    const userId = req.userId;
    if (req.userRole !== 'DRIVER') {
      res.status(400).json({message: 'Access forbidden'});
      return;
    }
    const message = await setActiveLoadNextState(userId);
    if (Array.isArray(message)) {
      res.status(200).json({message: message[1]});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.get('/:id', async (req, res) => {
  try {
    const userId = req.userId;
    const load = await getLoadById(req.params.id, userId,
        req.userRole === 'DRIVER');
    if (load === 400) {
      res.status(400).json({message: 'This is load of another user'});
      return;
    }
    if (!load) {
      res.status(400)
          .json({message: 'No load with id ' + req.params.id + ' found'});
      return;
    }
    res.status(200).json({load});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.put('/:id', async (req, res) => {
  try {
    const userId = req.userId;
    if (req.userRole !== 'SHIPPER') {
      res.status(400).json({message: 'Access forbidden'});
      return;
    }
    const message = await updateLoad(req.params.id, userId, req.body);
    if (message === 200) {
      res.status(200).json({message: 'Load details changed successfully'});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const userId = req.userId;
    if (req.userRole !== 'SHIPPER') {
      res.status(400).json({message: 'Access forbidden'});
      return;
    }
    const message = await deleteLoad(req.params.id, userId);
    if (message === 200) {
      res.status(200).json({message: 'Load deleted successfully'});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.post('/:id/post', async (req, res) => {
  try {
    const userId = req.userId;
    if (req.userRole !== 'SHIPPER') {
      res.status(400).json({message: 'Access forbidden'});
      return;
    }
    const message = await postLoad(req.params.id, userId);
    if (message === 200) {
      res.status(200).json({
        message: 'Load posted successfully',
        driver_found: true,
      });
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.get('/:id/shipping_info', async (req, res) => {
  try {
    const userId = req.userId;
    if (req.userRole !== 'SHIPPER') {
      res.status(400).json({message: 'Access forbidden'});
      return;
    }
    const load = await getLoadShippingInfo(req.params.id, userId);
    if (load === 400) {
      res.status(400).json({message: 'This is load of another user'});
      return;
    }
    if (load === 450) {
      res.status(400).json({message: 'This load is not assigned ' +
              'to driver yet or is already shipped'});
      return;
    }
    if (!load) {
      res.status(400)
          .json({message: 'No load with id ' + req.params.id + ' found'});
      return;
    }
    res.status(200).json({
      load: load[0],
      truck: load[1],
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

module.exports = router;
