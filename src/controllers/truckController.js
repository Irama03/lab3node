const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const {getTrucks, getTruckById, createTruck, updateTruck, deleteTruck,
  assignTruck} = require('../services/truckService');

router.get('/', async (req, res) => {
  try {
    const userId = req.userId;
    if (req.userRole !== 'DRIVER') {
      res.status(400).json({message: 'Access forbidden'});
      return;
    }
    const trucks = await getTrucks(userId);
    res.status(200).json({trucks});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.get('/:id', async (req, res) => {
  try {
    const userId = req.userId;
    if (req.userRole !== 'DRIVER') {
      res.status(400).json({message: 'Access forbidden'});
      return;
    }
    const truck = await getTruckById(req.params.id, userId);
    if (truck === 400) {
      res.status(400).json({message: 'This is truck of another driver'});
      return;
    }
    if (!truck) {
      res.status(400)
          .json({message: 'No truck with id ' + req.params.id + ' found'});
      return;
    }
    res.status(200).json({truck});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.post('/', async (req, res) => {
  try {
    const userId = req.userId;
    if (req.userRole !== 'DRIVER') {
      res.status(400).json({message: 'Access forbidden'});
      return;
    }
    const message = await createTruck(userId, req.body);
    if (message === 200) {
      res.status(200).json({message: 'Truck created successfully'});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.put('/:id', async (req, res) => {
  try {
    const userId = req.userId;
    if (req.userRole !== 'DRIVER') {
      res.status(400).json({message: 'Access forbidden'});
      return;
    }
    const message = await updateTruck(req.params.id, userId, req.body);
    if (message === 200) {
      res.status(200).json({message: 'Truck details changed successfully'});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const userId = req.userId;
    if (req.userRole !== 'DRIVER') {
      res.status(400).json({message: 'Access forbidden'});
      return;
    }
    const message = await deleteTruck(req.params.id, userId);
    if (message === 200) {
      res.status(200).json({message: 'Truck deleted successfully'});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.post('/:id/assign', async (req, res) => {
  try {
    const userId = req.userId;
    if (req.userRole !== 'DRIVER') {
      res.status(400).json({message: 'Access forbidden'});
      return;
    }
    const message = await assignTruck(req.params.id, userId);
    if (message === 200) {
      res.status(200).json({message: 'Truck assigned successfully'});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});


module.exports = router;
