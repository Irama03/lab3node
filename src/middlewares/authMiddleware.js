const jwt = require('jsonwebtoken');
const User = require('../models/userModel');

const verifyToken = async (req, res, next) => {
  const {authorization} = req.headers;
  if (!authorization) {
    return res.status(400).json({message: 'Authorization header is missed'});
  }
  const token = authorization.split(' ').pop();
  if (!token) {
    return res.status(400).json({message: 'Authorization token is missed'});
  }
  try {
    const user = jwt.verify(token, process.env.TOKEN_KEY);
    const u = await User.findById(user.user_id);
    if (!u) {
      return res.status(400).json({message: 'User has been deleted'});
    }
    req.userId = user.user_id;
    req.userRole = u.role;
  } catch (err) {
    return res.status(400).json({message: 'Invalid token'});
  }
  return next();
};

module.exports = verifyToken;
