/* eslint-disable require-jsdoc */
const User = require('../models/userModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const {getRandomString} = require('../utils/utils');
const {sendMail} = require('./emailService');
const saltRounds = 10;

async function createCredentials(body) {
  const schema = Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
    role: Joi.string().valid('SHIPPER', 'DRIVER').required(),
  });
  try {
    Joi.assert(body, schema);
  } catch (e) {
    return e.message;
  }
  const oldUser = await User.findOne({email: body.email});
  if (oldUser) {
    return 'User already exists. Please login';
  }
  const passwordEncrypted = await bcrypt.hash(body.password, saltRounds);
  const newUser = new User({
    email: body.email,
    password: passwordEncrypted,
    role: body.role,
  });
  newUser.save();
  return 200;
}

async function loginWithCredentials(body) {
  const schema = Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  });
  try {
    Joi.assert(body, schema);
  } catch (e) {
    return e.message;
  }
  const user = await User.findOne({email: body.email});
  if (!user) {
    return 'No user with such email found';
  }
  if (await bcrypt.compare(body.password, user.password)) {
    const token = jwt.sign(
        {
          user_id: user._id,
          email: body.email,
        },
        process.env.TOKEN_KEY,
        {
          expiresIn: '2h',
        },
    );
    return [token, ''];
  } else return 'Incorrect password';
}

async function sendPassword(body) {
  const schema = Joi.object().keys({
    email: Joi.string().email().required(),
  });
  try {
    Joi.assert(body, schema);
  } catch (e) {
    return e.message;
  }
  const u = await User.findOne({email: body.email});
  if (u) {
    const password = getRandomString(7);
    console.log('New password of user: ' + password);
    await sendMail([u.email], 'Please, change this password! ' +
        'Your temporary new password: ' + password);
    u.password = await bcrypt.hash(password, saltRounds);
    await u.save();
    return 200;
  }
  return 'No user with such email found';
}

module.exports = {createCredentials, loginWithCredentials, sendPassword};
