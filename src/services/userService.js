/* eslint-disable require-jsdoc */
const Joi = require('joi');
const User = require('../models/userModel');
const {driverIsOnLoad} = require('../services/truckService');
const bcrypt = require('bcrypt');
const saltRounds = 10;

async function getUserById(id) {
  const u = await User.findById(id);
  if (u) {
    return {
      '_id': u._id,
      'role': u.role,
      'email': u.email,
      'created_date': u.created_date,
    };
  }
  return u;
}

async function deleteUser(id, userIsDriver) {
  if (userIsDriver && await driverIsOnLoad(id)) {
    return 'Forbidden because of being on load';
  }
  const user = await User.findByIdAndDelete(id);
  if (user) return 200;
  return 'There is no info about this user';
}

async function updateUserPassword(id, body, userIsDriver) {
  if (userIsDriver && await driverIsOnLoad(id)) {
    return 'Forbidden because of being on load';
  }
  const schema = Joi.object().keys({
    oldPassword: Joi.string().required(),
    newPassword: Joi.string().required(),
  });
  try {
    Joi.assert(body, schema);
  } catch (e) {
    return e.message;
  }

  const user = await User.findById(id);
  if (user) {
    if (await bcrypt.compare(body.oldPassword, user.password)) {
      user.password = await bcrypt.hash(body.newPassword, saltRounds);
      await user.save();
      return 200;
    }
    return 'Incorrect old password';
  }
  return 'There is no info about this user';
}

async function getEmailsByUsersIds(ids) {
  const emails = [];
  for (const id of ids) {
    const u = await User.findById(id);
    if (u) emails.push(u.email);
  }
  return emails;
}

module.exports =
    {getUserById, deleteUser, updateUserPassword, getEmailsByUsersIds};
