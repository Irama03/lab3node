/* eslint-disable require-jsdoc */
const Load = require('../models/loadModel');
const Truck = require('../models/truckModel');
const Joi = require('joi');
const {sendMail} = require('./emailService');
const {getEmailsByUsersIds} = require('./userService');
const {dimensionsAreSuitable} = require('../utils/utils');

const statuses = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];
const states = ['En route to Pick Up', 'Arrived to Pick Up',
  'En route to delivery', 'Arrived to delivery'];
const truckTypes = {
  'SPRINTER': {
    width: 300,
    length: 250,
    height: 170,
    payload: 1700,
  },
  'SMALL STRAIGHT': {
    width: 500,
    length: 250,
    height: 170,
    payload: 2500,
  },
  'LARGE STRAIGHT': {
    width: 700,
    length: 350,
    height: 200,
    payload: 4000,
  },
};

async function getLoadsDriver(userId, status, offset, limit) {
  if (status === 'ALL') {
    return Load.find({assigned_to: userId}, '-__v')
        .skip(offset).limit(limit);
  }
  return Load.find({assigned_to: userId, status: status}, '-__v')
      .skip(offset).limit(limit);
}

async function getLoadsShipper(userId, status, offset, limit) {
  if (status === 'ALL') {
    return Load.find({created_by: userId}, '-__v')
        .skip(offset).limit(limit);
  }
  return Load.find({created_by: userId, status: status}, '-__v')
      .skip(offset).limit(limit);
}

const loadSchema = Joi.object().keys({
  name: Joi.string().required(),
  payload: Joi.number().greater(0).required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: Joi.required(),
});

const dimensionSchema = Joi.object().keys({
  width: Joi.number().greater(0).required(),
  length: Joi.number().greater(0).required(),
  height: Joi.number().greater(0).required(),
});

async function createLoad(userId, body) {
  try {
    Joi.assert(body, loadSchema);
    Joi.assert(body.dimensions, dimensionSchema);
  } catch (e) {
    return e.message;
  }
  const newLoad = new Load({
    created_by: userId,
    name: body.name,
    payload: body.payload,
    pickup_address: body.pickup_address,
    delivery_address: body.delivery_address,
    dimensions: body.dimensions,
  });
  await newLoad.save();
  console.log('Load \'' + newLoad.name + '\' created');
  return 200;
}

async function getActiveLoad(userId) {
  return Load.findOne({assigned_to: userId, status: statuses[2]}, '-__v');
}

async function setActiveLoadNextState(userId) {
  const l = await getActiveLoad(userId);
  if (l) {
    if (l.state === states[3]) {
      return 'Load has the last state that can not be increased';
    } else if (l.state === states[0]) l.state = states[1];
    else if (l.state === states[1]) l.state = states[2];
    else {
      l.state = states[3];
      l.status = statuses[3];
      l.assigned_to = ' ';
      const truck = await Truck.findOne({assigned_to: userId});
      if (truck) {
        truck.status = 'IS';
        await truck.save();
      } else throw Error('Load and driver do not have any truck');
    }
    const message = 'Load state changed to ' + l.state;
    l.logs.push({message: message});
    await l.save();
    const message1 = 'Load ' + l._id + ' with name \'' + l.name + '\'. ' +
        message;
    console.log(message1);
    await sendMail(await getEmailsByUsersIds([userId, l.created_by]), message1);
    return [200, message];
  }
  return 'No active load found';
}

async function getLoadById(id, userId, userIsDriver) {
  const l = await Load.findById(id, '-__v');
  if (l) {
    if (userIsDriver && userId === l.assigned_to.toString()) return l;
    if (!userIsDriver && userId === l.created_by.toString()) return l;
    return 400;
  }
  return l;
}

async function updateLoad(id, userId, body) {
  try {
    Joi.assert(body, loadSchema);
    Joi.assert(body.dimensions, dimensionSchema);
  } catch (e) {
    return e.message;
  }
  const l = await Load.findById(id);
  if (l) {
    if (userId === l.created_by.toString()) {
      if (l.status === statuses[0]) {
        l.name = body.name;
        l.payload = body.payload;
        l.pickup_address = body.pickup_address;
        l.delivery_address = body.delivery_address;
        l.dimensions = body.dimensions;
        await l.save();
        return 200;
      }
      return 'Forbidden to update - load state is not NEW';
    }
    return 'This is load of another shipper';
  }
  return 'No load with id ' + id + ' found';
}

async function deleteLoad(id, userId) {
  const l = await Load.findById(id);
  if (l) {
    if (userId === l.created_by.toString()) {
      if (l.status === statuses[0]) {
        await Load.findByIdAndDelete(id);
        return 200;
      }
      return 'Forbidden to delete - load state is not NEW';
    }
    return 'This is load of another shipper';
  }
  return 'No load with id ' + id + ' found';
}

async function postLoad(id, userId) {
  const l = await Load.findById(id);
  if (l) {
    if (userId === l.created_by.toString()) {
      if (l.status === statuses[0]) {
        l.status = statuses[1];
        let message = 'Load posted';
        l.logs.push({message: message});
        await l.save();
        let message1 = 'Load ' + l._id + ' with name \'' + l.name + '\'. ' +
            message;
        console.log(message1);
        await sendMail(await getEmailsByUsersIds([userId]), message1);

        const trucks = await Truck.find({assigned_to: {$ne: ' '},
          status: 'IS'});
        let t = null;
        for (const truck of trucks) {
          if (dimensionsAreSuitable(truckTypes[truck.type], l.payload,
              l.dimensions.width, l.dimensions.length, l.dimensions.height)) {
            t = truck;
            break;
          }
        }
        if (t) {
          t.status = 'OL';
          await t.save();
          l.status = statuses[2];
          l.state = states[0];
          l.assigned_to = t.assigned_to;
          message = 'Load assigned to driver with id ' + l.assigned_to;
          l.logs.push({message: message});
          await l.save();
          message1 = 'Load ' + l._id + ' with name \'' + l.name + '\'. ' +
              message;
          console.log(message1);
          await sendMail(await getEmailsByUsersIds([userId, l.assigned_to]),
              message1);
          return 200;
        }

        l.status = statuses[0];
        message = 'Load status returned to \'NEW\' because acceptable ' +
            'truck and driver have not been found';
        l.logs.push({message: message});
        await l.save();
        message1 = 'Load ' + l._id + ' with name \'' + l.name + '\'. ' +
            message;
        console.log(message1);
        await sendMail(await getEmailsByUsersIds([userId]), message1);
        return 'Acceptable truck and driver have not been found';
      }
      return 'This load is already posted';
    }
    return 'This is load of another shipper';
  }
  return 'No load with id ' + id + ' found';
}

async function getLoadShippingInfo(id, userId) {
  const l = await Load.findById(id, '-__v');
  if (l) {
    if (userId === l.created_by.toString()) {
      if (l.status === statuses[2]) {
        const t = await Truck.findOne({assigned_to: l.assigned_to}, '-__v');
        if (!t) throw Error('Load does not have any truck');
        return [l, t];
      }
      return 450;
    }
    return 400;
  }
  return l;
}

module.exports = {getLoadsDriver, getLoadsShipper, createLoad, getActiveLoad,
  setActiveLoadNextState, getLoadById, updateLoad, deleteLoad,
  postLoad, getLoadShippingInfo};
