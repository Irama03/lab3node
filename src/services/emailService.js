/* eslint-disable require-jsdoc */
const nodemailer = require('nodemailer');

async function sendMail(recipients, text) {
  console.log('Start sending email..');
  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
      user: 'autonotificationsbyirama@gmail.com',
      pass: 'aut0N0tificati0ns',
    },
  });
  for (const recipient of recipients) {
    const result = await transporter.sendMail({
      from: 'autonotificationsbyirama@gmail.com',
      to: recipient,
      subject: 'Notification from Trucks&Loads service',
      text: text,
    });
    console.log(recipient + ': ' + JSON.stringify(result));
  }
}

module.exports = {sendMail};
