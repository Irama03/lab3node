/* eslint-disable require-jsdoc */
const Truck = require('../models/truckModel');
const Joi = require('joi');

async function driverIsOnLoad(id) {
  return Truck.findOne({assigned_to: id, status: 'OL'});
}

async function getTrucks(userId) {
  return Truck.find({created_by: userId}, '-__v');
}

async function getTruckById(id, userId) {
  const t = await Truck.findById(id, '-__v');
  if (t) {
    if (userId === t.created_by.toString()) return t;
    return 400;
  }
  return t;
}

const truckSchema = Joi.object().keys({
  type: Joi.string().valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
      .required(),
});

async function createTruck(userId, body) {
  try {
    Joi.assert(body, truckSchema);
  } catch (e) {
    return e.message;
  }
  const newTruck = new Truck({
    created_by: userId,
    type: body.type,
  });
  await newTruck.save();
  return 200;
}

async function updateTruck(id, userId, body) {
  if (await driverIsOnLoad(userId)) return 'Forbidden because of being on load';
  try {
    Joi.assert(body, truckSchema);
  } catch (e) {
    return e.message;
  }
  const t = await Truck.findById(id);
  if (t) {
    if (userId === t.created_by.toString()) {
      t.type = body.type;
      await t.save();
      return 200;
    }
    return 'This is truck of another driver';
  }
  return 'No truck with id ' + id + ' found';
}

async function deleteTruck(id, userId) {
  if (await driverIsOnLoad(userId)) return 'Forbidden because of being on load';
  const t = await Truck.findById(id);
  if (t) {
    if (userId === t.created_by.toString()) {
      await Truck.findByIdAndDelete(id);
      return 200;
    }
    return 'This is truck of another driver';
  }
  return 'No truck with id ' + id + ' found';
}

async function assignTruck(id, userId) {
  const t = await Truck.findById(id);
  if (t) {
    if (t.assigned_to === userId) return 200;
    if (t.assigned_to === ' ') {
      const truck = await Truck.findOne({assigned_to: userId});
      if (!truck) {
        t.assigned_to = userId;
        await t.save();
        return 200;
      }
      if (truck.status === 'IS') {
        truck.assigned_to = ' ';
        await truck.save();
        t.assigned_to = userId;
        await t.save();
        return 200;
      }
      return 'Forbidden because driver is on load';
    }
    return 'This truck is already assigned';
  }
  return 'No truck with id ' + id + ' found';
}

module.exports =
    {driverIsOnLoad, getTrucks, getTruckById, createTruck,
      updateTruck, deleteTruck, assignTruck};
