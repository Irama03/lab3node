/* eslint-disable require-jsdoc */

function getRandomString(length) {
  let res = '';
  const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' +
      '0123456789!@#$%^&*?+-=';
  const charsLength = chars.length;
  for (let i = 0; i < length; i++) {
    res += chars.charAt(Math.random() * charsLength);
  }
  return res;
}

function dimensionsAreSuitable(dims, payload, width, length, height) {
  return (dims.payload >= payload &&
        ((dims.width >= width && dims.length >= length &&
                dims.height >= height) ||
            (dims.width >= width && dims.length >= height &&
                dims.height >= length) ||
            (dims.width >= length && dims.length >= width &&
                dims.height >= height) ||
            (dims.width >= length && dims.length >= height &&
                dims.height >= width) ||
            (dims.width >= height && dims.length >= width &&
                dims.height >= length) ||
            (dims.width >= height && dims.length >= length &&
                dims.height >= width)));
}

module.exports = {getRandomString, dimensionsAreSuitable};
