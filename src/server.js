/* eslint-disable require-jsdoc */
require('dotenv').config();
const PORT = process.env.PORT;
const PATH_TO_DB = process.env.PATH_TO_DB;

const express = require('express');
const morgan = require('morgan');
const server = express();
server.use(express.json());
server.use(morgan('combined'));

const mongoose = require('mongoose');
const {ServerApiVersion} = require('mongodb');

const authM = require('./middlewares/authMiddleware');

const users = require('./controllers/userController');
const trucks = require('./controllers/truckController');
const loads = require('./controllers/loadController');
const auth = require('./controllers/authController');
server.use('/api/users', authM, users);
server.use('/api/trucks', authM, trucks);
server.use('/api/loads', authM, loads);
server.use('/api/auth', auth);
server.use('*', authM, (req, res) => {
  res.status(200).json({message: 'Success'});
});

async function start() {
  await mongoose.connect(PATH_TO_DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    serverApi: ServerApiVersion.v1,
  }, (error) => {
    if (error) console.log('Error occurred when connecting to db');
  });
  server.listen(PORT, function() {
    console.log('Server is running on port ' + PORT);
  });
}

start();
