Trucks&Loads service

Project should be set up and launched with commands 'npm install' and 'npm start' just after git pull.

Except of required libraries, library 'nodemailer' for sending emails is used.

Filter by status and pagination of loads implemented.

Sending notifications through the email about shipment updates implemented.

Resetting password implemented. Can be reached by endpoint:
POST localhost:8080/api/auth/forgot_password
